from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from recipes.forms import RatingForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from recipes.models import Ingredient


# from recipes.forms import RecipeForm
from recipes.models import Recipe, ShoppingItem

# RecipeForm = None
# Recipe = None


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    fields = [
        "name",
        "servings",
        "description",
        "image",
    ]
    template_name = "recipes/new.html"
    success_url = "/"

    def form_valid(
        self, form
    ):  # gets the form, processes it, before sending to the database
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    fields = ["name", "servings", "description", "image"]
    template_name = "recipes/edit.html"
    success_url = reverse_lazy("recipes_list")


class RecipeListView(ListView):
    model = Recipe
    context_object_name = "recipes_list"
    template_name = "recipes/list.html"
    paginate_by = 3

    def get_queryset(
        self,
    ):  # get_queryset inheriting from ListView parent class
        query = self.request.GET.get("q")
        if not query:
            query = ""  # user input should be a string
        return Recipe.objects.filter(
            description__icontains=query,
        )  # from our list of all the recipes, filter out thru our description to see if it matches the query


class RecipeDetailView(DetailView):
    model = Recipe
    context_object_name = "recipe"
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        # Create a new empty list and assign it to a variable
        foods = []
        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()
        for item in self.request.user.shopping_item.all():
            # Add the shopping item's food to the list
            foods.append(item.food_item)
            context["servings"] = self.request.GET.get(
                "servings"
            )  #  Get the value out of there associated with the
        #   key "servings"
        # Store in the context dictionary with the key
        #   "servings"
        # Put that list into the context
        context["shopping_item_list"] = foods
        return context  # return the context that's used in the template


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = "/"


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        try:
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
        except Recipe.DoesNotExist:
            return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class ShoppingItemsListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"
    context_object_name = "shopping_item_list"


@login_required(login_url="login")
def create_shopping_item(request):
    user = request.user
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    ShoppingItem.objects.create(user=user, food_item=ingredient.food)
    return redirect("recipe_detail", pk=ingredient.recipe.id)


# get the current user
# get the ingredient id from the post
# get the specific ingredient from that ingredient model
# model.objects.get.("what i need")
# model.objects.create()
# redirect


def delete_shopping_item(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_item_list")


# def delete_shopping_item(request):
# should delete ShoppingItem instances associated with the current user.
# should show the empty shopping list after the deletion
# handles only HTTP POST requests


# ////////////////////////////////////////////////////////////////////////////////


def create_recipe(request):
    if request.method == "POST" and RecipeForm:
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save()
            return redirect("recipe_detail", pk=recipe.pk)
    elif RecipeForm:
        form = RecipeForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


def change_recipe(request, pk):
    if Recipe and RecipeForm:
        instance = Recipe.objects.get(pk=pk)
        if request.method == "POST":
            form = RecipeForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("recipe_detail", pk=pk)
        else:
            form = RecipeForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)


def show_recipes(request):
    context = {
        "recipes": Recipe.objects.all() if Recipe else [],
    }
    return render(request, "recipes/list.html", context)


def show_recipe(request, pk):
    context = {
        "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
        "rating_form": RatingForm(),
    }
    return render(request, "recipes/detail.html", context)

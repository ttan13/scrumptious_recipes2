from django import template

register = template.Library()


def resize_to(ingredient, new_servings):
    number_servings = ingredient.recipe.servings
    if number_servings is not None and new_servings is not None:
        try:
            ratio = int(new_servings) / number_servings
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount


register.filter(resize_to)

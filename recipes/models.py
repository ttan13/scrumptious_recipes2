from tkinter import CASCADE
from django.db import models
from django.template import Origin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    # author = models.CharField(max_length=50)
    author = models.ForeignKey(
        USER_MODEL, related_name="recipes", on_delete=models.CASCADE, null=True
    )
    description = models.TextField()  # unlimited
    image = models.URLField(
        null=True, blank=True
    )  # we can add one if wanted, but its ok if its empty
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    servings = models.IntegerField(
        validators=[MaxValueValidator(1000), MinValueValidator(1)], null=True
    )

    def __str__(self):  # changes 'recipe object1' to the actual recipe name
        return f"{self.name} by {str(self.author)}"


class Step(models.Model):
    order = models.SmallIntegerField()
    directions = models.TextField(max_length=300)
    recipe_name = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    food_items = models.ManyToManyField("FoodItem", blank=True)

    def __str__(self):
        return str(self.order) + ". " + self.directions


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):  # makes it easier to read in the admin
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    amount = models.IntegerField(
        default=1, validators=[MaxValueValidator(1000), MinValueValidator(0.1)]
    )
    measure = models.ForeignKey("Measure", on_delete=models.PROTECT)
    food = models.ForeignKey(
        "FoodItem", related_name="ingredients", on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.measure} of {str(self.food)}"


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ratings",
        on_delete=models.CASCADE,
    )


class ShoppingItem(models.Model):
    user = models.ForeignKey(
        USER_MODEL,
        related_name="shopping_item",
        on_delete=models.CASCADE,
        null=True,
    )
    food_item = models.ForeignKey(
        "FoodItem", related_name="food_item", on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.food_item}"

# Generated by Django 4.0.3 on 2022-06-02 22:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0010_tag'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Tag',
        ),
    ]

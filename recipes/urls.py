from django.urls import path

from recipes.views import (
    # create_recipe,
    # change_recipe,
    log_rating,
    # show_recipe,
    # show_recipes,
    RecipeListView,
    RecipeDetailView,
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDeleteView,
    ShoppingItemsListView,
    create_shopping_item,
    delete_shopping_item,
)

urlpatterns = [
    # path("", show_recipes, name="recipes_list"),
    # path("<int:pk>/", show_recipe, name="recipe_detail"),
    # path("new/", create_recipe, name="recipe_new"),
    # path("edit/", change_recipe, name="recipe_edit"),
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/update/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path(
        "shopping_items/create/", create_shopping_item, name="shopping_item_new"
    ),
    path(
        "shopping_items/",
        ShoppingItemsListView.as_view(),
        name="shopping_item_list",
    ),
    path(
        "shopping_items/delete/",
        delete_shopping_item,
        name="shopping_item_delete",
    ),
]

from distutils.log import Log
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import MealPlan


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    context_object_name = "meal_plan_list"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    # def get_queryset(
    #     self,
    # ):
    #     query = self.request.Get.get("q")
    #     if not query:
    #         query = ""
    #     return MealPlan.objects.filter()


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plan_detail")

    # def form_valid(
    #     self, form
    # ):  # when user saves, their user object should automatically save to the owner property of the mealplan
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)

    def form_valid(self, form):  # custom handling of saving the form
        plan = form.save(
            commit=False
        )  # save the meal plan, but dont put in the database
        plan.owner = self.request.user  # assign the owner to the meal plan
        plan.save()  # now save it to the database
        form.save_m2m()  # save all of the many-to-many relationships
        return redirect(
            "meal_plan_detail", pk=plan.id
        )  # redirect to the detail page for the meal plan


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    # success_url = reverse_lazy("meal_plan_detail")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

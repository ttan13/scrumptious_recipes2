from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.models import Recipe
from tags.models import Tag

# Tag = None


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    context_object_name = "tags"


class TagDetailView(DetailView):
    model = Tag
    context_object_name = "tag"
    template_name = "tags/detail.html"


class TagCreateView(LoginRequiredMixin, CreateView):
    model = Tag
    template_name = "tags/create.html"
    sucess_url = "/"


class TagUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "tags/edit.html"


class TagDeleteView(LoginRequiredMixin, DeleteView):
    model = Tag
    template_name = "tags/delete.html"


def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)

from django.urls import path

from tags.views import (
    # show_tags,
    TagListView,
    TagDetailView,
    TagCreateView,
    TagUpdateView,
    TagDeleteView,
)


urlpatterns = [
    # path("", show_tags, name="tags_list"),
    path("", TagListView.as_view(), name="tags_list"),
    path("<int:pk>/", TagDetailView.as_view(), name="tag_detail"),
    path("new/", TagCreateView.as_view(), name="tag_new"),
    path("<int:pk>/update/", TagUpdateView.as_view(), name="tag_edit"),
    path("<int:pk>delete/", TagDeleteView.as_view(), name="tag_delete"),
]
